import java.util.Scanner;
import java.util.Arrays;

class Program {
	public static void main(String[] args) {
	Scanner scanner = new Scanner(System.in);
			
		int n = scanner.nextInt();
		int array[] = new int[n];
			
		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
		}
		printArraySum(array);
		reversArray(array);
		arrayAverage(array);
		swapMinMax(array);
		bubble(array);
		arrayIntToInt(array);
	}
	public static int arrayIntToInt(int array[]) {
		int number = 0;

		for(int i = 0; i < array.length; i++) {
			number = number * 10 + array[i];
		}
		System.out.println(number);
		return number;
	}
	public static void bubble(int array[]) {
		int temp;

		for (int i = 0; i < array.length - 1; i++) {
        	for (int j = array.length - 1; j > i; j--) {
            	if (array[j - 1] > array[j]) {
                	temp = array[j - 1];
                	array[j - 1] = array[j];
                	array[j] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
	public static void swapMinMax(int numbers[]) {
		int temp;
		int minI = 0;
		int maxI = 0;
		int min = numbers[0];
		int max = numbers[0];

		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] < min) {
				min = numbers[i];
				minI = i;
			}
			if (numbers[i] > max) {
				max = numbers[i];
				maxI = i;
			}
		}
		temp = numbers[minI];
		numbers[minI] = numbers[maxI];
		numbers[maxI] = temp;

		System.out.println(Arrays.toString(numbers));
	}

	public static double arrayAverage(int array[]) {
		double arrayAverage = 0;
		double sum = 0;

		for (int i = 0; i < array.length; i++) {
			sum +=array[i];
		}
		arrayAverage = sum / array.length;
		System.out.println(arrayAverage);
		return arrayAverage;
	}

	public static void reversArray(int array[]) {
		int temp = 0;

		for (int j = 0; j < array.length/2; j++){
					
			temp = array[array.length - 1 - j];
			array[array.length -1 -j] = array[j];
			array[j] = temp;		
		}
		System.out.println(Arrays.toString(array));
	}

	public static int printArraySum(int array[]) {
		int arraySum = 0;
			
		for (int i = 0; i < array.length; i++){
			arraySum = arraySum + array[i];
		}
		System.out.println(arraySum);
		return arraySum;
	}
}
