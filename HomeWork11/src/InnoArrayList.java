
public class InnoArrayList implements InnoList {

    private static final int DEFAULT_SIZE = 10;

    private int elements[];

    private int count;

    public InnoArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        // TODO: реализовать
        if (count + 1 >= elements.length) {
            resize();
        }
        for (int i = count + 1; i >= index; i--) {
            elements[i] = elements[i - 1];
        }

        elements[index] = element;
        count++;
    }

    @Override
    public void addToBegin(int element) {
        // TODO: реализовать
        if (count == elements.length) {
            resize();
        }
        for (int i = count; i >= 0; i--) {
            elements[i + 1] = elements[i];
        }
        elements[0] = element;
        count++;
    }

    @Override
    public void removeByIndex(int index) {
        // TODO: реализовать
        for (int i = index; i < count; i++)
            elements[i] = elements[i + 1];
        elements[count] = 0;
        count--;

    }

    @Override
    public void add(int element) {
        // если список переполнен
        if (count == elements.length) {
            resize();
        }

        elements[count++] = element;
    }

    private void resize() {
        // создаем новый массив в полтора раза больший
        int newElements[] = new int[elements.length + elements.length / 2];
        // копируем из старого массива все элементы в новый
        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        // устанавливаем ссылку на новый массив
        this.elements = newElements;
    }

    @Override
    public void remove(int element) {
        // TODO: реализовать
        for (int i = 0; i < count; i++) {
            if (elements[i] == element) {
                elements[i] = 0;
                for (int k = i; k < count; k++) {
                    elements[k] = elements[k + 1];
                }
                count--;
            }
        }
    }

    @Override
    public boolean contains(int element) {
        // TODO: реализовать
        for (int i = 0; i < count; i++) {
            if (elements[i] == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        // возвращаем новый экземпляр итератора
        return new InnoArrayListIterator();
    }

    // внутренний класс позволяет инкапсулировать логику одного класса внутри класса
    private class InnoArrayListIterator implements InnoIterator {
        // текущая позиция итератора
        private int currentPosition;

        @Override
        public int next() {
            // берем значение под текущей позицией итератора
            int nextValue = elements[currentPosition];
            // увеличиваем позицию итератора
            currentPosition++;
            // возвращаем значение
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если текущая позиция не перевалила за общее количество элементов - можно дальше
            return currentPosition < count;
        }
    }
}
