public class Main {

    public static void main(String[] args) {
        Channel channel1 = new Channel("tnt");
        Channel channel2 = new Channel("tnv");
        Channel channel3 = new Channel("пятница");
        Channel channel4 = new Channel("супер");
        Channel channel5 = new Channel("tnt4");

        Program prog1 = new Program("передача 1");
        Program prog2 = new Program("передача 2");
        Program prog3 = new Program("передача 3");
        Program prog4 = new Program("передача 4");
        Program prog5 = new Program("передача 5");

        Tv tv = new Tv();

        tv.addChanel(channel1);
        tv.addChanel(channel2);
        tv.addChanel(channel3);
        tv.addChanel(channel4);
        tv.addChanel(channel5);

        channel2.addProg(prog1);
        channel2.addProg(prog2);
        channel2.addProg(prog3);
        channel2.addProg(prog4);

        RemoteController remoteController = new RemoteController();
        remoteController.setTv(tv);
        remoteController.changeChannel(1);

    }
}
