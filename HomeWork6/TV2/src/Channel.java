import java.util.Random;

public class Channel {
    private String nameChannel;
    private Program[] programs;
    private int progCount;
    private Random random;

    public Channel(String nameChannel) {
        this.nameChannel = nameChannel;
        this.progCount = 0;
        this.programs = new Program[3];
        this.random = new Random();
    }

    public void addProg(Program program) {
        if (progCount < programs.length) {
            programs[progCount] = program;
            progCount++;
        } else {
            System.out.println("Программы  переполены");
        }
    }

    public void channelProgram() {
        System.out.print(nameChannel + " " + "программа -");
        int randomProg = random.nextInt(progCount);
        programs[randomProg].show();
    }
}

