import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] numbersArrays = {333, 505, 77};
        String[] stringsArray = {"yt45rewq", "ry45re", "re54reR"};

        NumbersAndStringProcessor numbersAndStringProcessor = new NumbersAndStringProcessor(numbersArrays, stringsArray);

        StringProcess stringProcess = process -> {
            String result = "";
            for (int i = process.length() - 1; i >= 0; i--)
                if (Character.isAlphabetic(process.charAt(i))) {
                    result += process.charAt(i);
                }
            return result.toUpperCase();
        };
        System.out.println(Arrays.toString(numbersAndStringProcessor.processor(stringProcess)));

        NumbersProcess numbersProcess = number -> {
            int lastNum, sum = 0;
            while (number != 0) {
                if (number % 10 == 0) {
                    number /= 10;
                    continue;
                }
                lastNum = number % 10;

                if (lastNum % 2 != 0) {
                    lastNum -= 1;
                }
                number /= 10;

                sum = (sum * 10) + lastNum;
            }

            return sum;
        };
        System.out.println(Arrays.toString(numbersAndStringProcessor.processor(numbersProcess)));
    }
}
