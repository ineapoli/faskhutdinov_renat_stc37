
public class NumbersAndStringProcessor {
    private int arrayInt[];
    private String arrayString[];

    public NumbersAndStringProcessor(int arrayInt[], String arrayString[]) {
        this.arrayInt = arrayInt;
        this.arrayString = arrayString;
    }

    public int[] processor(NumbersProcess process) {
        int newArrayInt[] = new int[arrayInt.length];
        for (int i = 0; i < arrayInt.length; i++) {
            newArrayInt[i] = process.process(arrayInt[i]);
        }
        return newArrayInt;
    }

    public String[] processor(StringProcess process) {
        String newArrayString[] = new String[arrayString.length];
        for (int i = 0; i < arrayString.length; i++) {
            newArrayString[i] = process.process(arrayString[i]);
        }
        return newArrayString;
    }


}
