import java.util.Scanner;
import java.util.Arrays;

class Program4 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
			int n = scanner.nextInt();
			int numbers[] = new int[n];
			int temp;
			int minI = 0;
			int maxI = 0;

			for (int i = 0; i < numbers.length; i++){
				numbers[i] = scanner.nextInt();
			}

			int min = numbers[0];
			int max = numbers[0];

			for (int i = 0; i < numbers.length; i++) {
				if (numbers[i] < min) {
					min = numbers[i];
					minI = i;
				}
				if (numbers[i] > max) {
					max = numbers[i];
					maxI = i;
				}
			}
				temp = numbers[minI];
				numbers[minI] = numbers[maxI];
				numbers[maxI] = temp;

			System.out.println(Arrays.toString(numbers));
	}
}
