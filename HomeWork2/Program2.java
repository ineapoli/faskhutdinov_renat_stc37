import java.util.Scanner;
import java.util.Arrays;

class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];
		int temp;

		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
		}
		for (int j = 0; j < array.length/2; j++){
			temp = array[array.length - 1 - j];
			array[array.length -1 -j] = array[j];
			array[j] = temp;
		}
		System.out.println(Arrays.toString(array));
	}
}
