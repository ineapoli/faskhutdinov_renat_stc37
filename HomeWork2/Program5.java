import java.util.Scanner;
import java.util.Arrays;

class Program5 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
			int n = scanner.nextInt();
			int array[] = new int[n];
			int temp;

			for (int i = 0; i < array.length; i++){
				array[i] = scanner.nextInt();
				System.out.println(Arrays.toString(array));
			}
			for (int i = 0; i < array.length - 1; i++) {
            for (int j = array.length - 1; j > i; j--) {
                if (array[j - 1] > array[j]) {
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}
