public class Ellipse extends GeometricFigures  {
    protected double a;
    protected double b;

    public Ellipse(int x, int y, double a, double b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    public Ellipse(int x, int y, double a) {
        super(x, y);
        this.a = a;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    @Override
    public double area() {
        return Math.PI * a * b;
    }

    @Override
    public double perimeter() {
        return 4 * (Math.PI * a * b + (a - b) * (a - b)) / (a + b);
    }


    public void scale(double coefficient) {
        this.a *= coefficient;
        this.b *= coefficient;
    }

    public void move(int mX, int mY) {
        this.x += mX;
        this.y += mY;
    }
}


