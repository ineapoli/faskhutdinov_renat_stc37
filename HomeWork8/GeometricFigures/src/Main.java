public class Main {

    public static void main(String[] args) {
        Circle circle = new Circle(0, 0, 5);
        circle.move(4,0);
        circle.scale(2);
        System.out.println(circle.getB() + " площадь " + circle.area() + "  периметр " + circle.perimeter() + " кордината x = " + circle.x
         + " кордината y = " + circle.y);
        Rectangle rectangle = new Rectangle(0, 0, 5, 20);
        System.out.println(rectangle.area() + " " + rectangle.perimeter());
        Square square = new Square(0, 0, 5);
        System.out.println(square.area() + " " + square.perimeter());
        Ellipse ellipse = new Ellipse(0, 0, 5, 5);
        ellipse.scale(2);
        ellipse.move(2,2);
        System.out.println("площадь элипса =" + ellipse.area() + " периметр" + ellipse.perimeter() + "кардинаты x = " +
                ellipse.x + " y = " + ellipse.y);

    }
}
