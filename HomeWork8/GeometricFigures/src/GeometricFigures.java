public abstract class GeometricFigures implements Move,Scale {

    protected int x;
    protected int y;

    public GeometricFigures(int x, int y) {
        this.x = 0;
        this.y = 0;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public abstract double area();

    public abstract double perimeter();

    @Override
    public void move() {

    }

    @Override
    public void scale() {

    }
}


