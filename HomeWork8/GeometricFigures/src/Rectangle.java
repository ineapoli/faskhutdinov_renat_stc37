public class Rectangle extends GeometricFigures {
    protected double sideA;
    protected double sideB;


    public Rectangle(int x, int y, double sideA, double sideB) {
        super(x, y);
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public Rectangle(int x, int y, double sideA) {
        super(x, y);
        this.sideA = sideA;
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    @Override
    public double area() {
        return sideA * sideB;
    }

    @Override
    public double perimeter() {
        return (sideA + sideB) * 2;
    }

    public void move(int mX, int mY) {
        this.x += mX;
        this.y += mY;
    }


    public void scale(double coefficient) {
        this.sideA *= coefficient;
        this.sideB *= coefficient;
    }


    @Override
    public void scale() {

    }
}

