import java.util.Scanner;

class Program2 {
    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
        int array[] = {1, 7, 22, 28, 31, 33, 43, 55, 87, 99};
        int element = scanner.nextInt();
        int a = 0;
        int b = array.length - 1;
        if (binarySearch(element,array, a, b) == -1) {
            System.out.println("Element not present");
        }
        else {
            System.out.println("Element found");
        }
          
    }

    public static int binarySearch(int element, int array[], int left, int right) {
        //left = 0;
        //right = array.length - 1;
        int middle = (left + right)/2;
        
        if(right < left) {
            return -1;
        } 
        if (element < array[middle]) {
             return binarySearch(element, array, left, middle - 1);
        }
            
        if (element > array[middle]) {
            return binarySearch(element, array, middle + 1, right);
        }
            
        if (element == array[middle]) {
        return middle;
        }
        return -1;
    }
}
