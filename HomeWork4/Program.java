import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double n = scanner.nextInt();

		if (recursion(n) == 1) {
            System.out.println("Yes");
        } 
        else {
            System.out.println("No");
        }
	}
	public static int recursion(double n) {
		if (n == 1) {
			return 1;
		}
		else if (n > 1 && n < 2) {
            return 0;
        }
        else {
            return recursion(n / 2);
        }
	}
}
