public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    public static class builder {
        private User newUser;

        public builder() {
            newUser = new User();
        }

        public builder firstName(String firstName) {
            newUser.firstName = firstName;
            return this;
        }

        public builder lastName(String lastName) {
            newUser.lastName = lastName;
            return this;
        }

        public builder age(int age) {
            newUser.age = age;
            return this;
        }

        public builder isWorker(boolean isWorker) {
            newUser.isWorker = isWorker;
            return this;
        }

        public User build() {
            return newUser;
        }
    }
}
