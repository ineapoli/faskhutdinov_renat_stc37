public class Main {

    public static void main(String[] args) {
        User user = new User.builder()
                .firstName("Marsel")
                .lastName("Sidikov")
                .age(26)
                .isWorker(true)
                .build();
    }
}
